#!/usr/bin/python3
#
from artnet import artnet
from generator import cos
from generator import sin
from generator import tan
from osc import osc
import generic
import math
import time
#
a=artnet()
a.add("192.168.1.49")
a.display()
a.setchannel(1,255)
a.enable()
#x = cos(4.0)
#y = sin(4.0)
#z = tan(4.0)
####
pad=osc()
#pad.add(8001,ip="10.10.74.140")
pad.add(8001)
pad.display()
pad.enable()
d = 0.0
i = 0.0
gled = 0.0
i2 = 0.0
triangle = 0.0
intensity = 1.0
print("Starting Main Loop...")
while True:
    a.update()


    # Lets try an update from OSC to ARTNET
    if(pad.osc_values.get(b'/intensity/inten') != None):
        intensity = pad.osc_values.get(b'/intensity/inten')
    if(pad.osc_values.get(b'/intensity/d') != None):
        d = pad.osc_values.get(b'/intensity/d')
    if(pad.osc_values.get(b'/intensity/i') != None):
        i = pad.osc_values.get(b'/intensity/i')
    if(pad.osc_values.get(b'/intensity/g') != None):
        gled = pad.osc_values.get(b'/intensity/g')
    if(pad.osc_values.get(b'/intensity/i2') != None):
        i2 = pad.osc_values.get(b'/intensity/i2')
    if(pad.osc_values.get(b'/intensity/trian') != None):
        triangle = pad.osc_values.get(b'/intensity/trian')

    (r,g,b) = generic.hsv2rgb(d,intensity,1.0)
    a.setchannel(1, r)
    a.setchannel(2, g)
    a.setchannel(3, b)

    (r,g,b) = generic.hsv2rgb(i,intensity,1)
    a.setchannel(4, r)
    a.setchannel(5, g)
    a.setchannel(6, b)

    (r,g,b) = generic.hsv2rgb(gled,intensity,1)
    a.setchannel(7, r)
    a.setchannel(8, g)
    a.setchannel(9, b)

    (r,g,b) = generic.hsv2rgb(i2,intensity,1)
    a.setchannel(10, r)
    a.setchannel(11, g)
    a.setchannel(12, b)

    (r,g,b) = generic.hsv2rgb(triangle,intensity,1)
    a.setchannel(13, r)
    a.setchannel(14, g)
    a.setchannel(15, b)

        
    time.sleep(2.0/40.0)

