import socket
import sys
from struct import *
import time

class artnet():
    'Class to write out to an ArtNet Device'

    def __init__(self):
        print("Class called init")

    def add(self,ip, port=6454,universe=0):
        print("Adding a new Endpoint")
        self.ip = ip
        self.port = port
        self.universe = universe
        self.dmx = [0] * 512
        self.update_frequency = 40.0  # Base Frequency Update
        self.freq = 0.0   # Current Frequency of updates
        self.enabled = False
        self.sock = None
        self.message = None

    def display(self):
        print("Artnet: IP={}, port={}".format(self.ip,self.port))
        print("Update Frequency={}".format(self.freq))

    def setchannel(self,chan,dmxvalue=0):
        #print "Setting Channel=%d to Value=%d" % (chan,dmxvalue)
        cdmxvalue = int(dmxvalue)
        if((cdmxvalue < 0)|(cdmxvalue > 255)):
            #print "Error in dmx value"
            return()
        self.dmx[chan-1] = cdmxvalue

    def __create_packet(self):
        #message = 'Art-Net'
        ##OpCode 0x5000
        opcode=0x5000
        # ProtVer
        protocol_ver = 0x0e00
        # Sequence
        sequence = 0
        #message += '\x00'
        # Physical
        physical = 0
        # Universe
        universe = self.universe
        # Length - Set to 512
        dmx_length1=2
        dmx_length2=0
        #Format is GBR
        dmx = []
        dmx.append(b'A')
        dmx.append(b'r')
        dmx.append(b't')
        dmx.append(b'-')
        dmx.append(b'N')
        dmx.append(b'e')
        dmx.append(b't')
        dmx.append(0)
        dmx.append(opcode)
        dmx.append(protocol_ver)
        dmx.append(sequence)
        dmx.append(physical)
        dmx.append(universe)
        dmx.append(dmx_length1)
        dmx.append(dmx_length2)
        #
        for i in range(0,511):
            dmx.append(self.dmx[i])
        dmx.append(0) # investigate, not sure why we need to add another value here
        #print 'Creating Packet....'
        #message = pack('<sssssssBHHBBHBB BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB',*dmx)
        message = pack('<sssssssBHHBBHBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB',*dmx)
        return message

    def enable(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.freq = self.update_frequency
        self.enabled = True

    def disable(self):
        self.freq = 0.0  #disable updating
        if(self.sock != None):
            self.sock.close()
        self.enabled = False

    def update(self):
        'Create the actual packet and send to the end device'
        self.message = artnet.__create_packet(self)
        if (self.enabled):
            #print "Sending Packet..."
            try:
                sent = self.sock.sendto(self.message, (self.ip,self.port))
            except:
                print("Error Sending data to %s".format(self.ip))
