import math
import time

class cos():
    'Class to generate DMX values based on the COS equation'
    _initial_time = time.time()

    def __init__(self,period=1.0,amplitude=127.5,offset=127.5):
        print("COSINE generator called")
        self.period = period
        self.amplitude = amplitude
        self.offset = offset
        self.dmx = 0.0

    def update(self):
        delta_t = time.time() - cos._initial_time
        freq = delta_t*(360.0/self.period)
        ldmx = round(((math.cos(math.radians(freq)))*self.amplitude)+self.offset)
        if(ldmx < 0):
            ldmx = 0.0
        if(ldmx > 255):
            ldmx = 255.0
        self.dmx = int(ldmx)
        return(self.dmx)

class sin():
    'Class to generate DMX values based on the SIN equation'
    _initial_time = time.time()

    def __init__(self,period=1.0,amplitude=127.5,offset=127.5):
        print("SINE generator called")
        self.period = period
        self.amplitude = amplitude
        self.offset = offset
        self.dmx = 0.0

    def update(self):
        delta_t = time.time() - cos._initial_time
        freq = delta_t*(360.0/self.period)
        ldmx = round(((math.sin(math.radians(freq)))*self.amplitude)+self.offset)
        if(ldmx < 0):
            ldmx = 0.0
        if(ldmx > 255):
            ldmx = 255.0
        self.dmx = int(ldmx)
        return(self.dmx)

class tan():
    'Class to generate DMX values based on an equation'
    _initial_time = time.time()

    def __init__(self,period=1.0,amplitude=127.5,offset=127.5):
        print("TANGENT generator called")
        self.period = period
        self.amplitude = amplitude
        self.offset = offset
        self.dmx = 0.0

    def update(self):
        delta_t = time.time() - cos._initial_time
        freq = delta_t*(360.0/self.period)
        ldmx = round(((math.tan(math.radians(freq)))*self.amplitude)+self.offset)
        if(ldmx < 0):
            ldmx = 0.0
        if(ldmx > 255):
            ldmx = 255.0
        self.dmx = int(ldmx)
        return(self.dmx)

# lets also add Natural rhythm, square wave, and a sawtooth wave
