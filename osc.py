import socket
import sys
from struct import *
import time
import threading

class osc():
    'Class to receive from an OSC Device'

    def __init__(self):
        print("Class OSC Receive called")

    def add(self,port=8001,ip=""):
        print("Adding a new OSC Receive Endpoint")
        self.ip = ip
        self.port = port
        self.osc_values = {}
        self.update_frequency = 40.0  # Base Frequency Update
        self.freq = 0.0   # Current Frequency of updates
        self.enabled = False
        self.sock = None
        self.message = None

    def display(self):
        print("OSC Server: IP={}, port={}".format(self.ip,self.port))

    def _process_input(self,myname,*args):
        print("OSC Processing Input")
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        self.sock.bind((self.ip,self.port))
        while True:
            try:
                data,addr = self.sock.recvfrom(1024)
                #print ("Packet length is: {}".format(len(data)))
                if(len(data)>=2):
                    hdr = data.split(b'\0')[0]
                    ptype = data[len(hdr)+2:].split(b'\0')[0]
                    #hdr, ptype = unpack('!16s4s',data[:20])
                #print ("hdr={}".format(hdr.rstrip(b'\0')))
                #print ("ptype = {}".format(ptype.rstrip(b'\0')))

                message=data[len(hdr)+len(ptype)+4:]

                label = hdr.rstrip(b'\0')
                ptype = ptype.rstrip(b'\0')

                if (ptype == b',f'):
                    value = unpack('!1f',message)
                #print("Label:{} Value:{}".format(label,value[0]))
            
                self.osc_values[label]=value[0] # put value in osc dictionary
                #print("{}".format(self.osc_values))
            except:
                open("badpacket.dat","ab").write(data)  #write bad packets to file

    def enable(self):
        self.freq = self.update_frequency
        self.enabled = True
        self.listener = threading.Thread(target=self._process_input, args=(self,"OSC",1))
        self.listener.daemon=True
        self.listener.start()
        
    def disable(self):
        self.freq = 0.0  #disable updating
        #self.listener.exit()
        if(self.sock != None):
            self.sock.close()
        self.enabled = False
