#!/bin/sh/python
import socket
import httplib

UDP_IP = "0.0.0.0"
UDP_PORT = 6454

###
def huebridge(x,y,z):
	connection = httplib.HTTPConnection('192.168.1.106:80')
	body_content = "{\"on\":true, \"sat\":%s, \"bri\":%s,\"hue\":%s}"%(x,y,z)
	connection.request('PUT', '/api/newdeveloper/lights/1/state', body_content)
	result = connection.getresponse()
### DLS

##################
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sock.bind((UDP_IP, UDP_PORT))
print "Bound to Port"
lsat = 0
lbri = 0
lhue = 0
while True:
	##print "Wait..."
	data, addr = sock.recvfrom(2048)
	type1 = ord(data[8])
	type2 = ord(data[9])
	ptype = (type2*256)+type1
	if(ptype == 0x5000):
		sat = ord(data[18]) #channel 1
		bri = ord(data[19])
		hue = ord(data[20])*256
		print "Channel 1=%s Channel 2=%s, Channel 3=%s"%(sat,bri,hue)
		if((sat!=lsat)|(bri!=lbri)|(hue!=lhue)): # something has changed
			huebridge(sat,bri,hue)
			lsat = sat
			lbri = bri
			lhue = hue
			print "Channel 1=%s Channel 2=%s, Channel 3=%s"%(sat,bri,hue)
	##print data
##DLS