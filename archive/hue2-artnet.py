#!/bin/sh/python
import socket
import httplib

UDP_IP = "0.0.0.0"
UDP_PORT = 6454
lx=0.0
ly=0.0

###
def huebridge(red,green,blue): #in Red, Green, Blue
	global lx,ly
	#convert First ..  from RGB to XYZ, and then xy color point
	#http://www.everyhue.com/vanilla/discussion/94/rgb-to-xy-or-hue-sat-values/p1
	#https://github.com/PhilipsHue/PhilipsHueSDK-iOS-OSX/blob/master/ApplicationDesignNotes/RGB%20to%20xy%20Color%20conversion.md
	red = (red/255.0)
	green = (green/255.0)
	blue = (blue/255.0)
	#Gamma Correction
	if(red > 0.04045):
		red = pow((red + 0.055) / (1.0 + 0.055), 2.4)
	else:
		red =(red / 12.92)
	if(green > 0.04045):
		green = pow((green + 0.055) / (1.0 + 0.055), 2.4)
	else:
		green = (green / 12.92)
	if(blue > 0.04045):
		blue = pow((blue + 0.055) / (1.0 + 0.055), 2.4)
	else:
		blue = (blue / 12.92)
	##########
	#
	# Convert RGB to XYZ using D65 conversion formula
	dX = red * 0.649926 + green * 0.103455 + blue * 0.197109
	dY = red * 0.234327 + green * 0.743075 + blue * 0.022598
	dZ = red * 0.0000000 + green * 0.053077 + blue * 1.035763
	

	# now color point
	if(dX!=0 or dY!=0):
		x = dX / (dX + dY + dZ)
		y = dY / (dX + dY + dZ)
		dx=abs(lx-x)
		dy=abs(ly-y)
		#if((dx>=0.01) or (dy>=0.01)): # slow down the updates with larger deltas
		if(1==1):
			lx=x
			ly=y
			print "XY Channel x=%s, y=%s"%(x,y)
			connection = httplib.HTTPConnection('192.168.1.106:80')
			body_content = "{\"on\":true, \"xy\":[%s,%s], \"transitiontime\":0}"%(x,y)
			connection.request('PUT', '/api/newdeveloper/lights/1/state', body_content)
			result = connection.getresponse()
	else:
		connection = httplib.HTTPConnection('192.168.1.106:80')
		body_content = "{\"on\":false, \"transitiontime\":0}"
		connection.request('PUT', '/api/newdeveloper/lights/1/state', body_content)
		result = connection.getresponse()
### DLS

##################
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sock.bind((UDP_IP, UDP_PORT))
print "Bound to Port"
lred = 0
lgreen = 0
lblue = 0
while True:
	##print "Wait..."
	data, addr = sock.recvfrom(2048)
	type1 = ord(data[8])
	type2 = ord(data[9])
	ptype = (type2*256)+type1
	if(ptype == 0x5000):
		red = ord(data[18]) #channel 1
		green = ord(data[19])
		blue = ord(data[20])
		#print "RAW Channel 1=%s Channel 2=%s, Channel 3=%s"%(red,green,blue)
		if((red!=lred) or (green!=lgreen) or (blue!=lblue)): # something has changed
			huebridge(red,green,blue)
			lred = red
			lgreen = green
			lblue = blue
			print "Raw Channel 1=%s Channel 2=%s, Channel 3=%s"%(red,green,blue)
	#print data
#DLS
