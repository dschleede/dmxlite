#!/usr/bin/python
#
###############
#
# This program will talk to DMX lights over ArtNet
# Don Schleede - 2014
#
#######
import socket
import sys
from struct import *
import time


def create_packet(dd1,dd2,dd3):
	#message = 'Art-Net'
	##OpCode 0x5000
	opcode=0x5000
	# ProtVer
	protocol_ver = 0x0e00
	# Sequence
	sequence = 0
	#message += '\x00'
	# Physical
	physical = 0
	# Universe
	universe = 0

	# Length - Set to 512
	dmx_length1=2
	dmx_length2=0

	#Format is GBR

	dmx = []
	dmx.append('A')
	dmx.append('r')
	dmx.append('t')
	dmx.append('-')
	dmx.append('N')
	dmx.append('e')
	dmx.append('t')
	dmx.append(0)
	dmx.append(opcode)
	dmx.append(protocol_ver)
	dmx.append(sequence)
	dmx.append(physical)
	dmx.append(universe)
	dmx.append(dmx_length1)
	dmx.append(dmx_length2)
	#
	ch = 1
	for aa in range(1,10):
		dmx.append(dd2);ch+=1;
		dmx.append(dd3);ch+=1;
		dmx.append(dd1);ch+=1;
	
	
	for i in range(ch,513):
		dmx.append(0)
	#print dmx
	
	
	#print 'Creating Packet....'
	message = pack('<sssssssBHHBBHBB BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB',*dmx)

	return message


###Main
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_address = ('10.9.110.15',6454)
#server_address = ('10.10.16.115',6454)

#while 1:
	#for x in range(0,255):
message = create_packet(255,255,255)
#time.sleep(0.2)
sent = sock.sendto(message, server_address)

sock.close()
print len(message)
