#!/usr/bin/python3
#
"""
This program will walk a RGB white chain to show where the lights are
"""
#
from artnet import artnet
from generator import cos
from generator import sin
from generator import tan
from osc import osc
import generic
import math
import time
import os
#
# Lets set the serial port relay to white lights at the starts
#os.system("/bin/stty -F /dev/serial/port1 9600")
#time.sleep(1.0)
#os.system("/bin/echo A > /dev/serial/port1") time.sleep(1.0)
#os.system("/bin/echo a > /dev/serial/port1")
#time.sleep(1.0)
#
univ1=artnet()
univ1.add("192.168.1.49",6454,3)
univ1.display()
univ1.setchannel(1,255)
univ1.enable()
#
#x = cos(4.0)
#y = sin(4.0)
#z = tan(4.0)
####
pad=osc()
pad.add(8001)
pad.display()
pad.enable()
d = 0.0
i = 0.0
gled = 0.0
i2 = 0.0
triangle = 0.0
intensity = 1.0
relay = 0.0
oldrelay=0.0
print("Starting Main Loop...")
curr = 0
while True:
    univ1.update()

    for x in range(1,256):
        univ1.setchannel(x,0)
        
    r = 255
    g = 255
    b = 255
    univ1.setchannel(curr,r)
    univ1.setchannel(curr+1, g)
    univ1.setchannel(curr+2, b)
    curr = curr + 3
    time.sleep(1.0)
    print("{}".format((curr)/3.0))
#DLS
